# Mathematica Error Propagation Script

Use Mathematica to compute algebra while maintaining error propagation. Useful for manipulating measured data that contains uncertainty.

The script itself contains some useful examples on how to use it, maybe I'll add instructions to this README in the future.

This script was written by my Physics I TA, Alex.
